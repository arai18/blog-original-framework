<?php

class DbManager
{
    /**
     * @var array
     */
    protected $connections = [];

    /**
     * @var array
     */
    protected $repository_connection_map = [];

    /**
     * @var array
     */
    protected $repositories = [];

    /**
     * @param $name
     * @param $params
     */
    public function connect($name, $params)
    {
        $params = array_merge([
            'dsn'       => null,
            'user'      => '',
            'password'  => '',
            'options'   => [],
        ], $params);

        $con = new PDO(
            $params['dsn'],
            $params['user'],
            $params['password'],
            $params['options']
        );

        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->connections[$name] = $con;
    }

    /**
     * @param null $name
     * @return mixed
     */
    public function getConnection($name = null)
    {
        if ($name === null) {
            return current($this->connections);
        }

        return $this->connections[$name];
    }

    /**
     * @param $repository_name
     * @param $name
     */
    public function setRepositoryConnectionMap($repository_name, $name)
    {
        $this->repository_connection_map[$repository_name] = $name;
    }

    /**
     * @param $repository_name
     * @return mixed
     */
    public function getConnectionForRepository($repository_name)
    {
        if (isset($this->repository_connection_map[$repository_name])) {
            $name = $this->repository_connection_map[$repository_name];
            $con = $this->getConnection($name);
        } else {
            $con = $this->$this->getConnection();
        }

        return $con;
    }

    /**
     * @param $repository_name
     * @return mixed
     */
    public function get($repository_name)
    {
        if (isset($this->repositories[$repository_name])) {
            $repository_class = $repository_name . 'Repository';
            $con = $this->getConnectionForRepository($repository_name);

            $repository = new $repository_class($con);

            $this->repositories[$repository_name] = $repository;
        }

        return $this->repositories[$repository_name];
    }

    /**
     *
     */
    public function __destruct()
    {
        foreach ($this->repositories as $repository) {
            unset($repository);
        }

        foreach ($this->connections as $con) {
            unset($con);
        }
    }
}
