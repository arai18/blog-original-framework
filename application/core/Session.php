<?php

/**
 * Class Session
 */
class Session
{
    /**
     * @var bool
     */
    protected static $sessionStarted = false;

    /**
     * @var bool
     */
    protected static $sessionIdRegenerated = false;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        if (!self::$sessionStarted) {
            session_start();

            self::$sessionStarted = true;
        }
    }

    /**
     * @param $name
     * @param $value
     */
    public function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    /**
     * @param $name
     * @param null $default
     * @return mixed
     */
    public function get($name, $default = null)
    {
        if (isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
        return $default;
    }

    /**
     *
     */
    public function clear()
    {
        $_SESSION = [];
    }

    /**
     * @param bool $destroy
     */
    public function regenerate($destroy = true)
    {
        if (!self::$sessionIdRegenerated) {
            session_regenerate_id($destroy);

            self::$sessionIdRegenerated = true;
        }
    }

    /**
     * @param $bool
     */
    public function setAuthenticated($bool)
    {
        $this->set('_authenticated', (bool)$bool);

        $this->regenerate();
    }

    /**
     * @return mixed
     */
    public function isAuthenticated()
    {
        return $this->get('_authenticated', false);
    }
}
