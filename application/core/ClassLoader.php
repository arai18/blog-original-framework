<?php

class ClassLoader
{
    /**
     * @var
     */
    protected $dirs;

    /**
     *
     */
    public function register()
    {
        spl_autoload_register([
            $this,
            'loadClass'
            ]);
    }

    /**
     * @param $dir
     */
    public function registerDir($dir)
    {
        $this->dirs[] = $dir;
    }

    /**
     * @param $class
     */
    public function loadClass($class)
    {
        foreach ($this->dirs as $dir) {
            $file = $dir . '/' . $class . '.php';
            if (is_readable($file)) {
                require $file;

                return;
            }
        }
    }
}